# Spring cloud

- https://www.baeldung.com/spring-cloud-bootstrapping

```
v1-spring-cloud

git tag -a 1.0.1 -m "1.0.1" -m "1.0.1 - configuration server"


git tag -a service-discovery -m "service-discovery" -m "2.0 - Service Discovery implementation and apply on downstreams service"

```

## Agenda

- Configuration Server
- Service Discovery
  - with Spring Cloud Eureka of Netflix
- Open Feign Rest Client
- API Gateway
- Distributed Tracing
  - Distributed Tracing with Sleuth and Zipkin


## What the hell is this Spring Cloud?

- Is a framework for building robust cloud applications.


## Configuration Server

### Bootful Configuration


### Problems

### Solution

- https://docs.spring.io/spring-cloud-config/docs/current/reference/html/#_spring_cloud_config_server

---

## Service Discovery
